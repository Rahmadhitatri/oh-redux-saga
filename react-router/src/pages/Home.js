import React from 'react';

const Home = () => {
    return(
    <div className="App">
        <br />
        <br />
    <div className="container">
    <div className="jumbotron p-4 p-md-5 text-white rounded bg-dark">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol className="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div className="carousel-inner">
        <div className="carousel-item active">
          <img src="https://images.pexels.com/photos/1058959/pexels-photo-1058959.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="d-block w-100" alt="..." />
        </div>
        <div className="carousel-item">
          <img src="https://images.pexels.com/photos/450441/pexels-photo-450441.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="d-block w-100" alt="..." />
        </div>
        <div className="carousel-item">
          <img src="https://images.pexels.com/photos/1346184/pexels-photo-1346184.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" class="d-block w-100" alt="..." />
        </div>
      </div>
</div>
  </div>
  </div>
  <div className="container">
  <div className="row">
            <div className="col-md">
              <div className="card text-white bg-dark">
                <img src="https://images.pexels.com/photos/271639/pexels-photo-271639.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" className="card-img-top" alt="..." />
                <div className="card-body">
                  <p className="card-text">Recommend hotel with low budget.</p>
                </div>
              </div>
            </div>
            <div className="col-md">
              <div className="card text-white bg-dark">
                <img src="https://images.pexels.com/photos/1267473/pexels-photo-1267473.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" className="card-img-top" alt="..." />
                <div className="card-body">
                  <p className="card-text">Comfort place for healing.</p>
                </div>
              </div>
            </div>
            <div className="col-md">
              <div className="card text-white bg-dark">
                <img src="https://images.pexels.com/photos/3499599/pexels-photo-3499599.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" className="card-img-top" alt="..." />
                <div className="card-body">
                  <p className="card-text">Italian delight you must try.</p>
                </div>
              </div>
            </div>
          </div>
    </div>
    </div>
    )
};

export default Home;